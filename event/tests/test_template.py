from django.test import TestCase, Client

class test_templates(TestCase):

	def test_index(self):
		response = Client().get('/event/')
		self.assertTemplateUsed(response, 'event/index.html')

	def test_daftar(self):
		response = Client().get('/event/daftar')
		self.assertTemplateUsed(response, 'event/daftar.html')

	def test_tampil(self):
		response = Client().get('/event/tampil')
		self.assertTemplateUsed(response, 'event/tampil.html')