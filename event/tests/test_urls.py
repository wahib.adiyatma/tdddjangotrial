from django.test import TestCase, Client

class test_url(TestCase):

	def test_index(self):
		response = Client().get('/event/')
		self.assertEquals(200, response.status_code)

	def test_daftar(self):
		response = Client().get('/event/daftar')
		self.assertEquals(200, response.status_code)

	def test_tampil(self):
		response = Client().get('/event/tampil')
		self.assertEquals(200, response.status_code)