from django.test import TestCase, Client

class test_templates(TestCase):

	#def test_tampilan_index(self):
		#response = Client().get('/event/')
		#self.assertTemplateUsed(response, 'index.html')

	def test_tampilan_daftar(self):
		response = Client().get('/event/daftar')
		isi = response.content.decode('utf8')
		self.assertIn('<form method="POST">',isi)
		self.assertIn('Nama',isi)
		self.assertIn('Kegiatan',isi)

	def test_tampilan_tampil(self):
		response = Client().get('/event/tampil')
		isi = response.content.decode('utf8')
		self.assertIn(' <table class="table table-responsive-lg">',isi)
		self.assertIn('No', isi)
		self.assertIn('Pendaftar', isi)
		self.assertIn('Kegiatan', isi)
		



	