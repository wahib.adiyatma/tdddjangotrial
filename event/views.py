from django.shortcuts import render, redirect
from .forms import EventForm
from .models import EventModels

def index(request):
	context = {
		'judul':'Event Handler'
	}
	return render(request,'event/index.html', context)

def daftar(request):
	event_form = EventForm(request.POST or None)

	if request.method == 'POST':
		if event_form.is_valid():
			event_form.save()
			return redirect('event:index')

	context = {
		'event_form': event_form,
		'judul' : 'Daftar Event',
	}
	return render(request,'event/daftar.html', context)

def tampil(request):
	data_kegiatan = EventModels.objects.all()

	context = {
		'data_kegiatan': data_kegiatan,
		'judul' : 'Daftar Kegiatan',
	}

	return render(request,'event/tampil.html', context)