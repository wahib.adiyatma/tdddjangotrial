from django import forms
from .models import EventModels

class EventForm(forms.ModelForm):
	class Meta :
		model = EventModels
		fields = [
			'nama_peserta',
			'nomor_hp',
			'kegiatan',
			'alasan_ikut',
		]

		widgets = {
		'nama_peserta' : forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Input Nama Kamu'
				}
			),
		'nomor_hp': forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Input Nomor HP',
				}
			),
		'kegiatan' : forms.Select(
				attrs={
					'class':'form-control',
				}
		),
		'alasan_ikut' : forms.Textarea(
				attrs={
					'class':'form-control',
					'placeholder':'Kenapa ingin ikut acara ini ?',
				}
			),
		}
