from django.db import models

# Create your models here.
class EventModels(models.Model):
	KEGIATAN = (
		('Belajar PPW','Belajar PPW'),
		('Belajar SDA','Belajar SDA'),	
		('Tidur dan Rebahan','Tidur dan Rebahan'),				
		)

	nama_peserta = models.CharField(max_length=30)
	nomor_hp = models.CharField(max_length=30)
	kegiatan = models.CharField(
			max_length=60,
			choices = KEGIATAN,
			default = 'Belajar PPW',
		)


	alasan_ikut = models.TextField()



	def __str__(self):
		return "{}.{}".format(self.id,self.nama_peserta)