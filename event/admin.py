from django.contrib import admin

# Register your models here.
from .models import EventModels

admin.site.register(EventModels)
